    Big CTY - 26 January 2023
    Version entity is Malpelo Island, HK0/m

    Added/changed Entities/Prefixes/Callsigns:

	* EA8CHC/7 is Spain, EA
	* EA6XQ/P is Balearic Islands, EA6
	* JD1/JE7IZM and JE7IZM/JD1 are both Minami Torishima, JD/m
	* KB3WAV is Alaska, KL
	* LU3EU/E and LU6DTJ/D are both Argentina, LU
	* LU1XOP/W and LU8WVA/W are both Argentina, LU in ITU zone 16
	* TC100AGE, TC100HQ and TC100OLD are all Asiatic Turkey, TA
	* RQ9F/M is in ITU zone 30, not ITU zone 29
	* RQ9F/M is in CQ zone 17, not CQ zone 16
	* UE80SK is European Russia, UA
	* RX9WT/P is in CQ zone 16, not CQ zone 17
	* RI41POL is Asiatic Russia, UA9 in CQ zone 19

    Removed Entities/Prefixes/Callsigns:

	* EA6XQ/P in Canary Islands, EA8
	* RI41POL in European Russia, UA
